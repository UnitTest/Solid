﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lsp
{
    public interface ICliente
    {
        string Nombre { get; set; }
        double Monto { get; set; }
        string Imprimir();
    }

    public abstract class ClienteBase : ICliente
    {
        public string Nombre { get; set; }
        public double Monto { get; set; }
        public abstract string Imprimir();
    }

    public class ClienteOro : ClienteBase
    {

        public override string Imprimir()
        {
            return "Este es un cliente ORO cuyo nombre es: " + this.Nombre;
        }
    }

    public class ClientePremium : ClienteBase
    {

        public override string Imprimir()
        {
            return "Este es un cliente Premium cuyo nombre es: " + this.Nombre;
        }
    }

}
