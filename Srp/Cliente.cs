﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Srp
{
    public class Cliente
    {
        public string Nombre { get; set; }
        public string Apellido { get; set; }

        public int Add()
        {
            ClienteRepositorio clienteRepositorio = new ClienteRepositorio();
            try
            {
                return clienteRepositorio.Insert(Nombre, Apellido);
            }
            catch (Exception ex)
            {
                LogError(ex);
                throw;
            }

        }
        // añadir nueva funcionalidad de logging
        // sin cambiar la funcionalidad de las clases 

        private void LogError(Exception exception)
        {
            using (StreamWriter sw = new StreamWriter("@E:\\logs\\logfile.log"))
            {
                sw.WriteLine("{0} {1}", DateTime.Now.ToString(CultureInfo.InvariantCulture), exception.Message);
                sw.Close();
            }
        }
    }
}
