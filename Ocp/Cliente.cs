﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ocp
{
    public class Cliente
    {
        public string NombreCliente { get; set; }
        public double Monto { get; set; }
        public TipoCliente TipoCliente { get; set; }
        //Añadir tipos de cliente
        public double Descuento
        {
            get
            {
                if (TipoCliente == TipoCliente.Libre) return 0;
                return (Monto*0.10);
            }
        }


    }
}
